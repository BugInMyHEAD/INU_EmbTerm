#include <cmath>

#include "Point2d.h"


using namespace std;


Point2d::Point2d(int x, int y)
{
	setXY(x, y);
}


void Point2d::setXY(int x, int y)
{
	this->setX(x);
	this->setY(y);
}


void Point2d::setX(int x)
{
	this->x = x;
}


void Point2d::setY(int y)
{
	this->y = y;
}


int Point2d::getX() const
{
	return this->x;
}


int Point2d::getY() const
{
	return this->y;
}


void Point2d::setOrigin(const Point2d& origin)
{
	setX(this->getX() - origin.getX());
	setY(this->getY() - origin.getY());
}


double Point2d::getDistance(const Point2d& origin)
{
	double xdist = this->getX() - origin.getX();
	double ydist = this->getY() - origin.getY();

	return sqrt(xdist * xdist + ydist * ydist);
}
