#include "Room2d.h"


Room2d::Room2d(int x, int y, L2dNumeral w, L2dNumeral h)
{
	setXY(x, y);
	setWH(w, h);
}

Room2d::Room2d(const Point2d& pt2d, const Length2d& l2d)
	: Room2d(pt2d.getX(), pt2d.getY(), l2d.getW(), l2d.getH())
{
}


const Point2d& Room2d::getPoint2d() const
{
	return *this;
}


const Length2d& Room2d::getLength2d() const
{
	return *this;
}


bool Room2d::covers(const Point2d& pt2d) const
{
	int thisX = this->getX();
	int thisY = this->getY();
	int pt2dX = pt2d.getX();
	int pt2dY = pt2d.getY();

	return thisX <= pt2dX && pt2dX < thisX + (int)this->getW() &&
			thisY <= pt2dY && pt2dY < thisY + (int)this->getH();
}
