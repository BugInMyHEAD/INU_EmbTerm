#include "Length2d.h"


Length2d::Length2d(L2dNumeral w, L2dNumeral h)
{
	setWH(w, h);
}


void Length2d::updateArea()
{
	this->area = this->w * this->h;
}


void Length2d::setWH(L2dNumeral w, L2dNumeral h)
{
	this->w = w;
	this->h = h;
	updateArea();
}


void Length2d::setW(L2dNumeral w)
{
	this->w = w;
	updateArea();
}


void Length2d::setH(L2dNumeral h)
{
	this->h = h;
	updateArea();
}


L2dNumeral Length2d::getW() const
{
	return this->w;
}


L2dNumeral Length2d::getH() const
{
	return this->h;
}


long long Length2d::getArea() const
{
	return this->area;
}
