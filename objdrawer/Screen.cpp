#include <stdio.h>
#include <unistd.h> /* for open/close.. */
#include <fcntl.h> /* for O_RDONLY */
#include <sys/ioctl.h> /* for ioctl*/
#include <sys/mman.h> /* for mmap and munmap */
#include <linux/fb.h> /*for fb_var_screeninfo, FBIOGET_VSCREENINFO*/

#include "Screen.h"
#include "Canvas.h"


Screen::Screen(const char* fbFileName)
{
	this->fileDescr = open(fbFileName, O_RDWR);
	if (this->fileDescr < 0)
	{
		perror("fbdev open");
	}

	if (ioctl(
			this->fileDescr,
			FBIOGET_VSCREENINFO,
			&this->fbvar) < 0)
	{
		perror("fbdev ioctl");
	}
	if (this->fbvar.bits_per_pixel != 16)
	{
		fprintf(stderr, "bpp is not 16\n");
	}

	if (ioctl(this->fileDescr,
			FBIOGET_FSCREENINFO,
			&this->fbfix) < 0)
	{
		perror("fbdev ioctl");
	}

	this->mapBuf = (unsigned short *)
			mmap(
					0,
					this->fbfix.smem_len,
					PROT_READ | PROT_WRITE,
					MAP_SHARED,
					this->fileDescr,
					0);
	if (this->mapBuf == MAP_FAILED)
	{
		perror("MAP_FAILED");
	}
}


Canvas Screen::getCanvas()
{
    return Canvas(
			this->mapBuf,
			(L2dNumeral)this->fbvar.xres,
			(L2dNumeral)this->fbvar.yres);
}


Screen::~Screen()
{
	munmap(this->mapBuf, this->fbfix.smem_len);
	this->mapBuf = NULL;
	close(this->fileDescr);
	this->fileDescr = -1;
}
