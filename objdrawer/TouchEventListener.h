#pragma once


#include <iostream>
#include <string>
#include <functional>

#include "TouchEvent.h"
#include "Room2d.h"


using namespace std;


class TouchEventListener
{
private:

	Room2d r2d;
	std::function<void(const TouchEvent&)> action;

public:

	TouchEventListener(
			const Room2d& r2d, const std::function<void(const TouchEvent&)>& action);

	bool listenTo(const TouchEvent& te);
};
