#include "Pixel.h"


Pixel Pixel_make(unsigned char r, unsigned char g, unsigned char b)
{
	return (Pixel)(
			( ( r >> 3 ) << 11 ) |
					( ( g >> 2 ) << 5 ) |
					( b >> 3 ) );
}
