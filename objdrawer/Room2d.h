#pragma once


#include "Point2d.h"
#include "Length2d.h"


class Room2d : public Point2d, public Length2d
{
public:

	Room2d(int x, int y, L2dNumeral w, L2dNumeral h);
	Room2d(const Point2d& pt2d=Point2d(), const Length2d& l2d=Length2d());

	const Point2d& getPoint2d() const;
	const Length2d& getLength2d() const;
	bool covers(const Point2d& pt2d) const;
};
