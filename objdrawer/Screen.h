#pragma once


#include <linux/fb.h> /*for fb_var_screeninfo, FBIOGET_VSCREENINFO*/

#include "Pixel.h"
#include "Canvas.h"


class Screen
{
private:

	struct fb_fix_screeninfo fbfix;
	struct fb_var_screeninfo fbvar;
	int fileDescr;
	unsigned short * mapBuf;

public:

	virtual ~Screen();
	Screen(const char* fbFileName);

	Canvas getCanvas();
};
