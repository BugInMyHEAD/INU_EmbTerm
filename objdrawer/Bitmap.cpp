#include <iostream>
#include <stdio.h>
#include <climits>
#include <fstream>

#include "Bitmap.h"


using namespace std;


Bitmap::~Bitmap()
{
	delete[] this->buf;
}


Bitmap::Bitmap(const char* filename, L2dNumeral width, L2dNumeral height)
	: l2d(Length2d(width, height)), buf(new _32bpp[this->l2d.getArea()])
{
	ifstream fin;
	fin.open(filename);
	if (!fin)
	{
		cout << "Can't open " << filename << endl;
	}

	for (L2dNumeral i1 = 0; i1 < height; ++i1)
	{
		for (L2dNumeral i3 = 0; i3 < width; ++i3)
		{
			this->buf[i1 * width + i3].ch.a = UCHAR_MAX;
		}
	}

	for (L2dNumeral i1 = 0; i1 < height; ++i1)
	{
		for (L2dNumeral i3 = 0; i3 < width; ++i3)
		{
			int i5;
			fin >> i5;
			this->buf[i1 * width + i3].ch.r = (unsigned char)i5;
		}
	}

	for (L2dNumeral i1 = 0; i1 < height; ++i1)
	{
		for (L2dNumeral i3 = 0; i3 < width; ++i3)
		{
			int i5;
			fin >> i5;
			this->buf[i1 * width + i3].ch.g = (unsigned char)i5;
		}
	}

	for (L2dNumeral i1 = 0; i1 < height; ++i1)
	{
		for (L2dNumeral i3 = 0; i3 < width; ++i3)
		{
			int i5;
			fin >> i5;
			this->buf[i1 * width + i3].ch.b = (unsigned char)i5;
		}
	}

	fin.close();
}


const Length2d& Bitmap::getLength2d() const
{
	return this->l2d;
}


const _32bpp* Bitmap::getBuf() const
{
	return this->buf;
}
