#include <iostream>
#include "Oval.h"


using namespace std;


void Oval::drawImpl(Drawable<Pixel>& d)
{
	int startX = start.getX();
	int startY = start.getY();
	int startX2 = start.getX();
	int startY2 = start.getY();
	int prevX = prev.getX();
	int prevY = prev.getY();
	int nextX = next.getX();
	int nextY = next.getY();
	int t;

	Pixelmap& buffer = this->getBuffer();

	if (startX2 > prevX) {
		t = startX2;
		startX2 = prevX;
		prevX = t;
	}
	if (startY2 > prevY) {
		t = startY2;
		startY2 = prevY;
		prevY = t;

	}
	if (fill) {
		int width = (prevX - startX2) / 2;
		int height = (prevY - startY2) / 2;
		for (int i = -height; i<height; i++) {
			for (int j = -width; j<width; j++) {
				if (j*j * height*height + i*i * width * width <= height*height *width *width) {
					buffer.hlineLen(getBackup().getPixel(startX2 + width + j, startY2 + height + i), startY2 + height + i, startX2 + width + j, 1);
				}
			}
		}

	}
	else {
		int width = (prevX - startX2) / 2;
		int width2 = width - 1;
		int height = (prevY - startY2) / 2;
		int height2 = height - 1;

		double tmp0 = height * height * width * width;
		double tmp2 = height2 * height2 * width2 * width2;
		for (double i = -height; i<height; i++) {
			for (double j = -width; j<width; j++) {
				double tmp1 = j * j * height * height + i * i * width * width;
				if (tmp1 < tmp0 && tmp1 > tmp2) {
					//d.hlineLen(color, ypos1 + height + i, xpos1 + width + j, 1);
					buffer.hlineLen(getBackup().getPixel(startX2 + width + j, startY2 + height + i), startY2 + height + i, startX2 + width + j, 1);
				}
			}
		}

	}


	if (startX > nextX) {
		t = startX;
		startX = nextX;
		nextX = t;
	}
	if (startY > nextY) {
		t = startY;
		startY = nextY;
		nextY = t;

	}
	if (fill) {
		int width = (nextX - startX) / 2;
		int height = (nextY - startY) / 2;
		for (int i = -height; i<height; i++) {
			for (int j = -width; j<width; j++) {
				if (j*j * height*height + i*i * width * width <= height*height *width *width) {
					buffer.hlineLen(color, startY + height +  i , startX + width + j , 1);
				}
			}
		}

	}
	else {
		int width = (nextX - startX) / 2;
		int width2 = width - 1;
		int height = (nextY - startY) / 2;
		int height2 = height - 1;
	
		double tmp0 = height * height * width * width;
		double tmp2 = height2 * height2 * width2 * width2;
		for (double i = -height; i<height; i++) {
			for (double j = -width; j<width; j++) {
				double tmp1 = j * j * height * height + i * i * width * width;
				if (tmp1 < tmp0 && tmp1 > tmp2) {
					//d.hlineLen(color, ypos1 + height + i, xpos1 + width + j, 1);
					buffer.hlineLen(color, startY + height + i, startX + width + j, 1);
				}
			}
		}
	}

	int minX;
	int minY;
	int maxX;
	int maxY;
	if (startX < startX2)
	{
		minX = startX;
	}
	else
	{
		minX = startX2;
	}
	if (startY < startY2)
	{
		minY = startY;
	}
	else
	{
		minY = startY2;
	}
	if (nextX > prevX)
	{
		maxX = nextX;
	}
	else
	{
		maxX = prevX;
	}
	if (nextY > prevY)
	{
		maxY = nextY;
	}
	else
	{
		maxY = prevY;
	}
	d.printDrawable(minX, minY, buffer, minX, minY, maxX, maxY);
}
