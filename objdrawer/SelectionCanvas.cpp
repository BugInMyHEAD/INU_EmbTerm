#include "SelectionCanvas.h"


SelectionCanvas::~SelectionCanvas()
{
}


SelectionCanvas::SelectionCanvas()
	: Canvas(), sh(Length2d())
{
}


SelectionCanvas::SelectionCanvas(const Canvas& c)
	: SelectionCanvas(c, Room2d(Point2d(), c.getLength2d()))
{
}


SelectionCanvas::SelectionCanvas(const Canvas& c, const Room2d& r2d)
	: Canvas(c, r2d), sh(r2d.getLength2d())
{
}

SelectionHelper& SelectionCanvas::getSelectionHelper()
{
	return sh;
}
