#pragma once


#include "Canvas.h"
#include "Layer.h"


class SelectionHelper : public Drawable<Layer*>
{
public:

	virtual ~SelectionHelper();
	SelectionHelper(const Length2d& l2d);

	void overwriteLayerPtr(const SelectionHelper& sh);
	void overwriteLayerPtr(Layer& l);
	void overwriteLayerPtr(Layer& l, Layer* pl);
};
