#include "Shape.h"
#include "Point2d.h"
#include "Layer.h"
#include <iostream>


using namespace std;


Shape::Shape()
	:
	start(INT_MIN, INT_MIN, INT_MIN),
	prev(INT_MIN, INT_MIN, INT_MIN),
	next(INT_MIN, INT_MIN, INT_MIN)
{
}


Shape::~Shape()
{
	if (backup)
	{
		delete backup;
	}
}


const Pixelmap& Shape::getBackup() const
{
	return *this->backup;
}

Pixelmap & Shape::getBuffer()
{
	return *this->buffer;
}

bool Shape::isActive() const
{
	return this->active;
}


void Shape::drawline(
		Drawable<Pixel>& d,
		const Point2d& start,
		const Point2d& next,
		Pixel color)
{
	int xpos1 = start.getX();
	int ypos1 = start.getY();
	int xpos2 = next.getX();
	int ypos2 = next.getY();
	float x2 = (float)xpos1;
	float y2 = (float)ypos1;
	float dx = (xpos2 - xpos1) / 1000.f;
	float dy = (ypos2 - ypos1) / 1000.f;

	for (int i = 0; i < 1000; i++)
	{
		x2 += dx;
		y2 += dy;
		if (x2 < 0 || y2 < 0)
		{
			break;
		}
		d.hlineLen(color, (int) y2, (int) x2, 1);
	}
}


void Shape::draw(
		Drawable<Pixel>& d,
		const TouchEvent& te,
		Pixel color,
		bool fill)
{
	if (te.getPressure() > 0 && !active)
	{
		this->start = this->next = te;
		this->backup = new Pixelmap(d.getLength2d());
		this->backup->printDrawable(d);
		this->buffer = new Pixelmap(*this->backup);
	}
	this->prev = this->next;
	this->next = te;
	this->fill = fill;
	this->color = color;
	if (te.getPressure() == 0)
	{
		this->active = false;
		delete this->backup;
		this->backup = nullptr;
		delete this->buffer;
		this->buffer = nullptr;

		return;
	}
	drawImpl(d);
	this->active = true;
}
