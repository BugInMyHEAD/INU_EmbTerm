#include "SelectionPixelmap.h"


SelectionPixelmap::SelectionPixelmap(const Pixelmap& pm)
	: Pixelmap(pm), sh(pm.getLength2d())
{
}


SelectionPixelmap::SelectionPixelmap(const Bitmap& bm)
	: SelectionPixelmap(bm.getLength2d())
{
}


SelectionPixelmap::SelectionPixelmap(L2dNumeral width, L2dNumeral height)
	: SelectionPixelmap(Length2d(width, height))
{
}


SelectionPixelmap::SelectionPixelmap(const Length2d& l2d)
	: Pixelmap(l2d), sh(l2d)
{
}

SelectionHelper& SelectionPixelmap::getSelectionHelper()
{
	return sh;
}
