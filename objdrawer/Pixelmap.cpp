#include <iostream>
#include <memory.h>

#include "Pixelmap.h"


Pixelmap::~Pixelmap()
{
	delete[] this->buf;
}


Pixelmap::Pixelmap(const Pixelmap& pm)
	: Drawable<Pixel>(new Pixel[pm.getLength2d().getArea()], pm.getLength2d())
{
	this->printDrawable(pm);
}


Pixelmap::Pixelmap(const Bitmap& bm)
	: Pixelmap(bm.getLength2d())
{
	size_t height = this->l2d.getH();
	size_t width = this->l2d.getW();
	const _32bpp* bmbuf = bm.getBuf();
	for (size_t i1 = 0; i1 < height; i1++)
	{
		for (size_t i3 = 0; i3 < width; i3++)
		{
			size_t i4 = i1 * width + i3;
			this->buf[i4] = Pixel_make(bmbuf[i4].ch.r, bmbuf[i4].ch.g, bmbuf[i4].ch.b);
		}
	}
}


Pixelmap::Pixelmap(L2dNumeral width, L2dNumeral height)
	: Pixelmap(Length2d(width, height))
{
}


Pixelmap::Pixelmap(const Length2d& l2d)
	: Drawable<Pixel>(new Pixel[l2d.getArea()], l2d)
{
}


Pixel* Pixelmap::getBuf()
{
	return this->buf;
}


const Pixel* Pixelmap::getBuf() const
{
	return this->buf;
}


Pixel Pixelmap::getPixel(L2dNumeral x, L2dNumeral y) const
{

	return this->buf[y * this->l2d.getW() + x];
}
