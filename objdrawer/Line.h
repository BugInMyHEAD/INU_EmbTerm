#include "Shape.h"


class Line : public Shape
{
protected:

	virtual void drawImpl(Drawable<Pixel>& d);
};
