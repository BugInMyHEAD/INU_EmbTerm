#pragma once


#include "Pixelmap.h"
#include "Point2d.h"
#include "Drawable.h"
#include "Layer.h"
#include "TouchEvent.h"
#include "Canvas.h"


class Shape
{
private:

	Pixelmap * backup = nullptr;
	Pixelmap * buffer = nullptr;
	bool active = false;

protected:

	TouchEvent start, prev, next;
	Pixel color;
	bool fill = false;

	virtual void drawImpl(Drawable<Pixel>& d) = 0;
	const Pixelmap& getBackup() const;
	Pixelmap& getBuffer();
	bool isActive() const;

public:

	Shape();
	virtual ~Shape();

	void drawline(
			Drawable<Pixel>& d,
			const Point2d& start,
			const Point2d& next,
			Pixel color); 
	void draw(
			Drawable<Pixel>& d,
			const TouchEvent& te,
			Pixel color,
			bool fill);
};
