#include <iostream>
#include <algorithm>
#include "Rectangle.h"


using namespace std;

int Rectangle::sign(int a){
	if (a <0 )
		return -1;
	else 
		return 1;
}
void Rectangle::drawImpl(Drawable<Pixel>& d)
{
	int startX = start.getX();
	int startY = start.getY();
	int startX2 = start.getX();
	int startY2 = start.getY();
	int prevX = prev.getX();
	int prevY = prev.getY();
	int nextX = next.getX();
	int nextY = next.getY();
	int t;
	if (startX2 > prevX) {
		t = startX2;
		startX2 = prevX;
		prevX = t;
	}
	if (startY2 > prevY) {
		t = startY2;
		startY2 = prevY;
		prevY = t;

	}
	if (fill) {
	
		//if (!( abs(tnextx >=tprevx) && abs(tnexty >= tprevy)) || sign(tprevx) != sign(tnextx) || sign(tprevy) != sign(tnexty)){	
	
			
			d.printDrawable(startX2, startY2, getBackup() , startX2, startY2 , prevX, prevY);

		//}

			
	}

	
	else {
	

		//d.hlineLen(color, startY, startX, nextX - startX);

		for (int i= 0; i<prevX-startX2; i++){
		d.hlineLen(getBackup().getPixel(startX2+i, startY2 ), startY2,  startX2 +i, 1);
		}

		//d.hlineLen(color, nextY, startX, nextX - startX);
		for (int i=0; i<prevX-startX2; i++){
		d.hlineLen(getBackup().getPixel(startX2 + i , prevY ),  prevY  , startX2 +i, 1);
		}

		for (int i1 = startY2; i1 < prevY; ++i1)
		{
			d.hlineLen(getBackup().getPixel(startX2 , i1), i1, startX2, 1);
			d.hlineLen(getBackup().getPixel(prevX ,  i1) , i1, prevX, 1);
		} 

	}
	

	 
	 if (startX > nextX) {
		 t = startX;
		 startX = nextX;
		 nextX = t;
	 }
	 if (startY > nextY) {
		 t = startY;
		 startY = nextY;
		 nextY = t;

	 }
	
	if (fill) {
	
		for (int i1=startY; i1<nextY; ++i1)
			d.hlineLen(color, i1, startX, nextX - startX);
	}
	else {
		d.hlineLen(color, startY, startX, nextX - startX);
		d.hlineLen(color, nextY, startX, nextX - startX);

		for (int i1 = startY; i1 < nextY; ++i1)
		{
			d.hlineLen(color, i1, startX, 1);
			d.hlineLen(color, i1, nextX, 1);
		}
	}
}
