#include "Shape.h"


class Eraser : public Shape
{
protected:

	virtual void drawImpl(Drawable<Pixel>& d);


	void drawline2(Drawable<Pixel>& d, const Point2d& start, const Point2d& next, Pixel color);
};
