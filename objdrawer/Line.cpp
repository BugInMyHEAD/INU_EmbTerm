#include <iostream>
#include "Line.h"

using namespace std;
void Line::drawImpl(Drawable<Pixel>& d)
{
	
	int xpos1 = start.getX();
	int ypos1 = start.getY();
	int xpos2 = prev.getX();
	int ypos2 = prev.getY();
	float x2 = (float)xpos1;
	float y2 = (float)ypos1;
	float dx = (xpos2 - xpos1) / 1000.f;
	float dy = (ypos2 - ypos1) / 1000.f;

	for (int i = 0; i < 1000; i++)
	{
		x2 += dx;
		y2 += dy;
		if (x2 < 0 || y2 < 0)
		{
			break;
		}
		d.hlineLen(getBackup().getPixel((int)x2, (int) y2 ), (int) y2, (int) x2, 1);
	}

	drawline(d,start.getPoint2d(), next.getPoint2d(), color);
}
