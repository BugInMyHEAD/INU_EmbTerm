#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

#include "TouchScreen.h"
#include "TouchEvent.h"

TouchScreen::TouchScreen(const char* fbFileName, const char* eventFileName)
	: Screen(fbFileName), te(INT_MIN, INT_MIN, INT_MIN)
{
	int fd = open(eventFileName, O_RDWR);
	if (fd < 0)
	{

	}
	this->eventFileDescr = fd;

	int XD0 = 25;
	int YD0 = 25;
	int XD1 = 100;
	int YD1 = 100;
	int XD2 = 50;
	int YD2 = 150;
	int X0 = 456;
	int Y0 = 3549;
	int X1 = 1321;
	int Y1 = 2393;
	int X2 = 741;
	int Y2 = 1565;
	this->K = (X0 - X2)*(Y1 - Y2) - (X1 - X2)* (Y0 - Y2);
	this->A = ((XD0 - XD2)*(Y1 - Y2) - (XD1 - XD2)*(Y0 - Y2)) / K;
	this->B = ((X0 - X2)*(XD1 - XD2) - (XD0 - XD2)*(X1 - X2)) / K;
	this->C = (Y0*(X2*XD1 - X1*XD2) + Y1*(X0*XD2 - X2*XD0) + Y2* (X1 * XD0 - X0*XD1)) / K;
	this->D = ((YD0 - YD2)*(Y1 - Y2) - (YD1 - YD2)*(Y0 - Y2)) / K;
	this->E = ((X0 - X2)*(YD1 - YD2) - (YD0 - YD2)*(X1 - X2)) / K;
	this->F = (Y0*(X2*YD1 - X1*YD2) + Y1 * (X0*YD2 - X2*YD0) + Y2 * (X1*YD0 - X0 * YD1)) / K;
}

TouchScreen::~TouchScreen()
{
	close(this->eventFileDescr);
}

TouchEvent TouchScreen::waitForTouchEvent()
{
	do
	{
		struct input_event ie;
		read(this->eventFileDescr, &ie, sizeof(ie));

		if (ie.type == 3)
		{
			switch (ie.code)
			{
			case 0:
				this->rawX = ie.value;
				break;
			case 1:
				this->rawY = ie.value;
				break;
			case 24:
				this->te.setPressure(ie.value);
				break;
			}
		}
	} while (this->te.getPressure() < 0);
	Point2d& teCoord = this->te.getPoint2d();
	teCoord.setX((int)(A * this->rawX + B * this->rawY + C));
	teCoord.setY((int)(D * this->rawX + E * this->rawY + F));

	return this->te;
}


std::vector<TouchEventListener>& TouchScreen::getTouchEventListenerVector()
{
	return this->telVector;
}


TouchEvent TouchScreen::wakeTouchEventListner()
{
	this->waitForTouchEvent();
	this->wakeTouchEventListner(this->te);

	return this->te;
}


void TouchScreen::wakeTouchEventListner(const TouchEvent& te)
{
	Point2d pt2d(te.getPoint2d());
	for (auto& var : this->telVector)
	{
		if (var.listenTo(te))
		{
		}
	}
}
