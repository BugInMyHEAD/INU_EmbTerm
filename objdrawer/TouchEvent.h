#pragma once


#include "Point2d.h"


class TouchEvent : public Point2d
{
private:

	int pressure;

public:

	TouchEvent(const Point2d& pt2d, int pressure);
	TouchEvent(int x, int y, int pressure);

	const Point2d& getPoint2d() const;
	Point2d& getPoint2d();
	void setPoint2d(const Point2d& pt2d);
	int getPressure() const;
	void setPressure(int pressure);
};
