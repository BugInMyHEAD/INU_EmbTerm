CFLAGS = -std=c++11 -Wall

INC = include objdrawer/*.h
SRC = objdrawer/*.cpp

cross: $(SRC) $(INC)
	arm-linux-gnueabihf-g++ $(CFLAGS) $(SRC) -o objdrawer.exe
	tar -zcf embterm.tar.gz Makefile include objdrawer ui.txt objdrawer.exe
	
native: $(SRC) $(INC)
	g++ $(CFLAGS) $(SRC) -o objdrawer.exe

clean:
	rm objdrawer.exe embterm.tar.gz
